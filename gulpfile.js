/*==========================================================
Author: Andrea S
==========================================================*/

/** List Gulp dependencies **/
var $                   = require('gulp-load-plugins')();
    gulp                = require('gulp'),
    sass                = require('gulp-sass'),
    prefix              = require('gulp-autoprefixer'),
    sourcemaps          = require('gulp-sourcemaps'),
    concat              = require('gulp-concat'),
    uglify              = require('gulp-uglify'),
    plumber             = require('gulp-plumber'),
    notify              = require("gulp-notify"),
    imagemin            = require('gulp-imagemin'),
    pngquant            = require('imagemin-pngquant'),
    del                 = require('del'),
    gulpif              = require('gulp-if'),
    cssmin              = require('gulp-cssmin'),
    useref              = require('gulp-useref'),
    data                = require('gulp-data'),
    kit                	= require('gulp-kit-2'),
    mustache 			= require('gulp-mustache');
    refresh 			= require('gulp-refresh');

var browserSync         = require('browser-sync').create()

var prod = true, /* var for production mode */
    dest = './', /* Folder destination project */
    destHtml = 	dest,
    destCss = 	dest + '/assets/css',
    destJs = 	dest + '/assets/js',
    destImg = 	dest + '/assets/img',
    destFonts = dest + '/assets/fonts'


/**
*
* Styles
* - Catch errors (gulp-plumber)
* - Compile with 'compressed' output if prod
* - Autoprefixer
*
**/
gulp.task('styles', function() {
    del.sync(destCss);
    gulp.src('_dev/scss/**/*.scss')
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sass({outputStyle: prod ? 'compressed' : 'expanded'})) /* compressed */
        .pipe(prefix())
        .pipe(sourcemaps.write())
        //.pipe(cssmin())
        .pipe(gulp.dest(destCss))
        .pipe(browserSync.stream());
});


/**
*
* Fonts
* - copy assets in destination folder
*
**/
gulp.task('fonts', function() {
    del.sync(destFonts);
    gulp.src('_dev/fonts/**/*')
        .pipe(gulp.dest(destFonts))
        .pipe(browserSync.stream());
});


/**
*
* Scripts
* - Catch errors (gulp-plumber)
* - Concat
* - Uglify if prod
*
**/
gulp.task('scripts', function() {
    del.sync(destJs);
    gulp.src('_dev/js/**/*.js')
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        //.pipe(concat('scripts.js'))
        .pipe(gulpif(prod, uglify()))
        .pipe(gulp.dest(destJs))
        .pipe(browserSync.stream());
});


/**
*
* Images
* - Image optimization if prod
*
**/
gulp.task('images', function () {
    del.sync(destImg);
    gulp.src('_dev/img/**/*')
        .pipe(gulpif(prod, imagemin({
            progressive: true,
            use: [pngquant()]
        })))
        .pipe(gulp.dest(destImg));
});


/**
*
* Templates
* - copy html
* - minify html if prod
*
**/
gulp.task('templates', function() {
    return gulp.src('_dev/templates/*.kit')
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe( kit() )
    .pipe(mustache('_dev/templates/data.json',{},{}))
    .pipe(gulp.dest(destHtml))
    .pipe(browserSync.stream());
});


/**
*
* Watch assets
*
**/
gulp.task('watch', function() {
    gulp.watch('_dev/templates/**/*.html', ['templates']);
    gulp.watch('_dev/scss/**/*.scss', ['styles']);
    gulp.watch('_dev/js/**/*.js', ['scripts']);
    gulp.watch('_dev/img/**/*', ['images']);
    gulp.watch('_dev/fonts/**/*', ['fonts']);
});


/**
*
* Build task
*
**/
gulp.task('build', function() {
    gulp.start('styles', 'scripts', 'images', 'fonts', 'templates');
});


/**
*
* Production
*
**/
gulp.task('production', ['styles', 'scripts', 'images', 'fonts', 'templates'], function() {
    gulp.src(['_dev/templates/**/*.html', '!_dev/templates/**/_*.html', '!_dev/templates/{_*,_*/**}'])
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', cssmin()))
        .pipe(gulp.dest(dest));
});


/**
* Gulp Prod
* - build all the assets in production env
*/
gulp.task('prod', function(){
    prod = true;
    gulp.start('production');
});

/**
*
* Serve - BrowserSync.io
* - Watch CSS, JS, IMG & HTML for changes
* - View project at: localhost:3000
*
**/
gulp.task('serve', ['build'], function() {
    browserSync.init({
        server: {
            proxy: "yourlocal.dev" 
        },
        ui: {
		    port: 8080
		},
        notify: false
    });
    gulp.watch('_dev/img/**/*', ['images']);
    gulp.watch('_dev/scss/**/*.scss', ['styles']);
    gulp.watch('_dev/js/**/*.js', ['scripts']);
    gulp.watch('_dev/fonts/**/*', ['fonts']);    
    gulp.watch('_dev/templates/**/*', ['templates']).on('change', browserSync.reload);
});


/**
*
* Default task
* - launch serve
*
**/
gulp.task('default', ['serve']);

