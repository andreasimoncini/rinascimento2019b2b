$(document).ready(function(){
    
    ///// MAIN + HEADER  ------- 

    $(function () {
        $('[data-toggle="popover"]').popover()
    })
    $("html").on("mouseup", function (e) {
        var l = $(e.target);
        if (l[0].className.indexOf("popover") == -1) {
            $(".popover").each(function () {
                $(this).popover("hide");
            });
        }
    });


    //Sticky header
    $(function() {
        var header = $(".header");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if ( $("body").hasClass( "home" ) ) {
                if (scroll >= 200) {
                   // header.addClass("is-sticky");
                } else {
                   // header.removeClass("is-sticky");
                }
            }
        });
    });

    // checkbox prezzi
    function togglePrices() {
        var checkPrices = $('input#chkbx-prezzi');
        checkPrices.change(function(){
            if ($(this).is(':checked')) {
                // prezzi visibili
                $("body").removeClass("hide-prices");
            } else {
                // prezzi non visibili
                $("body").addClass("hide-prices");
            }
        });

    }
    togglePrices();


    // Hide 
    $("a.close").click(function(){
	    $(this).parent().fadeOut( "slow" );
    });
    $("a.js-close").click(function(){
	    $(this).parent().fadeOut( "slow" );
    });
    $(".header__banner a.close").click(function(){
        $(this).parent().fadeOut('slow', function() {
            $('body').removeClass("banner-active");
        });
    });
    $(".cookienotice a.btn").click(function(){
	    $(".cookienotice").fadeOut( "slow" );
    });

    // SEARCH   

    $(".icon--search a").click(function(){
	    $(".submenu__search").slideToggle();
    });
    $("a.search__close").click(function(){
	    $(".submenu__search").slideUp();
    });


    //MEGA MENU img switch

    $('.header__submenu .submenu__items a').hover(
        function(){
            var linkIndex = $(this).attr("data-filename");
            //$(this).addClass('hover');
            $('.box__image').attr('src', linkIndex);
        },
        function(){
            //$(this).removeClass('hover');
            //$('.box__image').attr('src', 'default.jpg');
        }
    );

    // MOBILE MENU   
    /*
    $(".mobile_menu").slideMobileMenu({
        "hamburgerId"   : "sm_menu_ham", // Hamburger Id
        "hamCloseId"    : "sm_menu_ham_open", //Hamburger Close Id
        "hamWrapId"     : "icon--menu__toggle", // Hamburger Wrapper Id
        "wrapperClass"  : "sm_menu_outer",  // Menu Wrapper Class
        "submenuClass"  : "submenu",  // Submenu Class
        "onMenuLoad"    : function() { return true; }, // Calls when menu loaded
        "onMenuToggle"  : function() { return true; } // Calls when menu open/close
    });
    */
    $("#icon--menu__toggle").click(function(){
        $('.mobile_menu_outer').addClass( "active" );
        $('body').addClass( "no-scroll" );
    });
    $("#sm_menu_ham_open").click(function(){
        $('.mobile_menu_outer').removeClass( "active" );
        $('body').removeClass( "no-scroll" );
    });
    $(".mobile_menu li a").click(function(e){
        $(e.target).parent().toggleClass( "active" );
    });
    
        
    // CART  
    $(".js-opencart").click(function(){
        $(this).next('.quick-cart').toggleClass( "active" );
        $('body').toggleClass( "no-scroll" );
    });
        
    $(".js-custom-scrollbars").mCustomScrollbar({
        axis:"y",
        theme: "dark-thick"
    });


    ///// FOOTER ------- 

    //Footer Newsletter subscription
    $('input#inputNewsletter').focus(function() {
        $('.newsletter-sub__extra').fadeIn('slow');
        $(this).focus();
    });

    $(".footer-toggle").click(function(){
        if ( $( this ).parent().hasClass( "open" ) ) {
            $(this).parent().toggleClass( "open" );
        } else {
            $(".footer__menu li").removeClass("open");
	        $(this).parent().toggleClass( "open" );
        }
        
    });

    //Footer Collapse Spedizioni e Lingua 
    $('a#js-fclshipping').click(function () {
        $('html, body').animate({
            scrollTop: $("#footerCollapse").offset().top
        }, 800);
    });
    
    $('a#js-fcllanguage').click(function () {
        $('html, body').animate({
            scrollTop: $("#footerCollapse").offset().top
        }, 800);
    });

    // Back to top 
    $("#back2top").hide();
    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 700) {
                jQuery('#back2top').fadeIn();
            } else {
                jQuery('#back2top').fadeOut();
        
            }
        });


        // scroll body to 0px on click
        $('a#back2top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    ///// HOME ------- 

    //PLAY VIDEO FROM IMAGE SPLASHSCREEN
    $('.play-video').click(function(){
        video = '<iframe src="'+ $(this).attr('data-video') +'"></iframe>';
        $(this).replaceWith(video);
    });
    
    // SLIDER
    
    $('.hero--slider').owlCarousel({
        loop:true,
	    margin:0,
	    items: 1,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        lazyLoad: true,
        autoHeight: true
	
    });
    
    $('.carousel-menu').owlCarousel({
	    loop:false,
	    margin:10,
	    items: 1,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        lazyLoad: true
	
    });
    $('.carousel-1').owlCarousel({
	    loop:false,
	    margin:0,
        nav:true,
        dots: false,
        lazyLoad: true,
        autoHeight: true,
        items:1
    });
    $('.carousel-product').owlCarousel({
	    loop:false,
	    margin:0,
        nav:false,
        dots: true,
        lazyLoad: true,
        items:1
    });

    $('.carousel-2').owlCarousel({
	    loop:false,
        margin:20,
        nav:true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            990:{
                items:2
            },
            1024:{
                items:2
            },
            1280:{
                items:2
            }
        },
        autoWidth:false
    });

    $('.carousel-3').owlCarousel({
	    loop:false,
        margin:20,
        nav:true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:1.5
            },
            768:{
                items:2
            },
            990:{
                items:3
            }
        },
        autoWidth:true
    });

    $('.carousel-3-1').owlCarousel({
	    loop:false,
        margin:20,
        nav:true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            990:{
                items:3
            }
        },
        autoWidth:true
    });

    $('.carousel-4-1').owlCarousel({
	    loop:false,
	    margin:30,
        nav:true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            990:{
                items:3
            },
            1024:{
                items:3
            },
            1280:{
                items:4
            }
        }
	
    });
    
    $('.carousel-5').owlCarousel({
	    loop:false,
	    margin:20,
        nav:true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:2.3
            },
            768:{
                items:2
            },
            990:{
                items:3
            },
            1024:{
                items:4
            },
            1280:{
                items:5
            }
        }
	
    });
    $('.carousel-5-1').owlCarousel({
	    loop:false,
	    margin:30,
        nav:true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            990:{
                items:3
            },
            1024:{
                items:3
            },
            1280:{
                items:5
            }
        }
	
    });
    
    $('.carousel-7').owlCarousel({
	    loop:false,
	    margin:20,
        nav:true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:2.3
            },
            768:{
                items:2
            },
            990:{
                items:3
            },
            1024:{
                items:5
            },
            1280:{
                items:7
            }
        }
	
	});

    ///// LISTING  PAGE -------     
    $(".filter-toggle").click(function(){
        if ( $( this ).parent().hasClass( "open" ) ) {
            $(this).parent().toggleClass( "open" );
        } else {
            //$(".filters__menu li").removeClass("open");
	        $(this).parent().toggleClass( "open" );
        }
        
    });


    $(".qb__image-thumb").click(function(){
        var linkIndex = $(this).attr("data-target");
        $('.qb__image-main').attr('src', linkIndex);
    });
    
    // dropdown - aggiorna label on click
    $(".dropdown-menu div a").click(function(){
        $(this).parents(".dropdown").find('.dropdown-toggle').html($(this).text());
        $(this).parents(".dropdown").find('.dropdown-toggle').val($(this).data('value'));
    });

    $('.js-listing-view.go-small').click(function(){
        $('.js-listing-view').removeClass('active');
        $(this).addClass('active')
        $('.listing__products .col-6').removeClass('col-md-4').addClass('col-md-3');
    });
    $('.js-listing-view.go-big').click(function(){
        $('.js-listing-view').removeClass('active');
        $(this).addClass('active')
        $('.listing__products .col-6').removeClass('col-md-3').addClass('col-md-4');
    });

    //Listing FILTERS MOBILE
    
    $(".js-toggleLFM").click(function(){
	    $("#listingFiltersModal").fadeIn( "fast" );
    });
    $(".js-toggleLOM").click(function(){
	    $("#listingOrderModal").fadeIn( "fast" );
    });
    if ( $( ".filter-priceRange" ).length ) {
        new JSR(['.priceRangeMin', '.priceRangeMax'], {
            sliders: 2,
            min: 0,
            max: 1000,
            step: 5,
            values: [0, 1000],
            labels: { 
                formatter: function (value) {
                    return '€' + value.toString();
                }
            },

        });
    }
    if ( $( ".filter-priceRange-mobile" ).length ) {
        new JSR(['.priceRangeMin', '.priceRangeMax'], {
            sliders: 2,
            min: 0,
            max: 1000,
            step: 5,
            values: [0, 1000],
            labels: { 
                formatter: function (value) {
                    return '€' + value.toString();
                }
            },

        });
    }



    $( "#listingFiltersModal .accordion__row" ).each(function( index ) {
        var mainLabel = $(this).find(".accordion__toggle");
        var check = $(this).find("input[type=checkbox]");
        

        check.change(function(){
            if ($(this).is(':checked')) {
                $(this).parent('label').addClass('is-checked');
                mainLabel.addClass('is-checked');
            } else {
                $(this).parent('label').removeClass('is-checked');
            }
           
        });
        check.click(function(){
            var numChecked = $(this).is(':checked').length;
            console.log("check " +  numChecked );
            if (numChecked < 1) {
                mainLabel.removeClass('is-checked');
            }
        });
    });


    // ** SINGLE PRODUCT PAGE **
    //$(".sticky_column").stick_in_parent({offset_top: 220});
    var window_width = jQuery( window ).width();

    if (window_width < 768) {
        $(".sticky_column").trigger("sticky_kit:detach");
    } else {
      make_sticky();
    }

    jQuery( window ).resize(function() {

      window_width = jQuery( window ).width();

      if (window_width < 768) {
        $(".sticky_column").trigger("sticky_kit:detach");
      } else {
        make_sticky();
      }

    });

    function make_sticky() {
      $(".sticky_column").stick_in_parent({
        offset_top: 0,
        parent: ".sticky_parent"
      });
    }

    $(function () {
        $('.size-selector').click(function () {
            $(this).toggleClass('active');
            //$(this).next('ul.size-list').toggleClass('hidden');
            $('ul.size-list').toggleClass('hidden');
        });
    });
    $( ".size-list input" ).click(function(){
        var value = $(this).val();
        if ($(this).is(':checked')) {
            $('.size-selector__txt').text(value);
            $('.size-selector').toggleClass('active');
            $('ul.size-list').toggleClass('hidden');
        } 
    });

    $(document).mouseup(function (e) {
        if (!$('ul.size-list').is(e.target) && $('ul.size-list').has(e.target).length === 0) {
            $('ul.size-list').addClass('hidden');
            $('.size-selector').removeClass('active');
        }
    });


    if( $(window).width() > 480 && (!navigator.userAgent.match(/Android/i) ||
    !navigator.userAgent.match(/webOS/i) ||
    !navigator.userAgent.match(/iPhone/i) ||
    !navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/))
    ){

        $('[data-fancybox^="gallery"]').fancybox({
            keyboard: true,
            smallBtn: false,
            arrows: false,
            infobar: true,
            toolbar: true,
            baseClass: "fancybox-white",
            iframe : {
                preload : false
            },
            wheel: false,
            thumbs : {
                autoStart : true,
                axis: "y"
            },
            buttons : [
                'close'
            ],
            afterShow: function(instance) {
                instance.scaleToActual();
                $(".fancybox-image").click()
            }
        });

    }


    // share links
    $(".js-show-share-links").click(function(){
        $(".share-links").toggle("slow");
    });


    // sticky product 
    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        
        return elementBottom > viewportTop && elementTop < viewportBottom;
    };
    if( $('.product__section').length ) {
        $(window).on('resize scroll', function() {
            
            if ($('.product__section').isInViewport()) {
                $('.sticky-product').addClass('hidden');
            } else {
                $('.sticky-product').removeClass('hidden');
            }
        });
    }


    // SIZE SELECTOR
    $("input.size__input").change(function() {
        $(this).css({"background-color": "#000", "color": "#fff"});
    });

    

    // LOOKBOOK

    $(".item__pic").click(function(){
        if ( $(this).parent().next(".item__collect").hasClass( "is-visible" ) ) {
            $(this).parent().next(".item__collect").removeClass( "is-visible" );
        } else {
            $(this).toggleClass( "is-open" );
            $(".item__collect").removeClass( "is-visible" );
            $(this).parent().next(".item__collect").addClass( "is-visible" );
        }
        
    });

    //** lookbook nested items: calcolo dimensioni immagini */
    function resizeNestedItems(){
        if($('.lookbook__products').length >0 ){
            $( ".item--nested .item__wrap" ).each(function() {
                var $width = $(this).find( ".item__image--main" ).css('width');
                $( this ).css('width', $width ).addClass( "mod" );
                console.log($width);
            });
        }
    }
    function resetNestedItems(){
        if($('.lookbook__products').length >0 ){
            $( ".item--nested .item__wrap" ).each(function() {
                $( this ).css('width','100%' ).addClass( "mod" );
            });
        }
    }
/* 
    if (window_width > 1024) {
        resizeNestedItems();
    }
    $(window).resize(function() { 
        if (window_width > 1024) {
            resizeNestedItems();
        } else {
            resetNestedItems();
        }
    });  */
    
    


    // Fidelity CARDA Area Riservata -	ruler
	// math per spostare indicatore punti						
    var punti = $('#ruler-punti__pointer').data('punti');
        xVal = 0;
	
	function spostaPointer(npunti) {
		
			 if (punti <= 0) { xVal = 6.7 }
		else if (punti >= 1   && punti < 34)  { xVal = 6.7 }
		else if (punti >= 34  && punti < 67)  { xVal = 12 }
		else if (punti >= 67  && punti < 101) { xVal = 26.5 }
		else if (punti >= 101 && punti < 201) { xVal = 33.6 }
		else if (punti >= 201 && punti < 301) { xVal = 45 }
		else if (punti >= 301 && punti < 401) { xVal = 52.5 }
		else if (punti >= 401 && punti < 501) { xVal = 60.5 }
		else if (punti >= 501 && punti < 601) { xVal = 72 }
		else if (punti >= 601 && punti < 701) { xVal = 78 }
		else if (punti >= 701 ) { xVal = 87.4 }
		
		$('#ruler-punti__pointer').css("left", xVal + "%");

	}
    spostaPointer();



    
// document.ready
}); 


// STORE LOCATOR
$(window).on("load",function(){
    
    /* TABLE TAGLIE */
    $(function() {
        $(".table-taglie").delegate('td','mouseover mouseleave', function(e) {
            if (e.type == 'mouseover') {
            $(this).parent().addClass("hover");
            $("colgroup").eq($(this).index()).addClass("hover");
            } else {
            $(this).parent().removeClass("hover");
            $("colgroup").eq($(this).index()).removeClass("hover");
            }
        });
    });

});
